#[macro_use]
extern crate log;
extern crate pancurses;

use pancurses::Input;

const LOCATION_SIZE: usize = 21;
const LOCATION_DRAW_X_OFFSET: i32 = 1;
const LOCATION_DRAW_Y_OFFSET: i32 = 1;
const LOCATION_DRAW_X_SCALE: i32 = 2;
const MESSAGES_DRAW_X_OFFSET: i32 = 1;
const MESSAGES_DRAW_Y_OFFSET: i32 = 3;
const MESSAGE_LOG_CAPACITY: usize = 6;

fn draw_terrain(terrain: &Vec<char>, window: &pancurses::Window)
{
    let mut x = 0;

    for c in terrain {
        window.mvprintw(
            LOCATION_DRAW_Y_OFFSET,
            LOCATION_DRAW_X_OFFSET + (x * LOCATION_DRAW_X_SCALE),
            c.to_string(),
        );
        x += 1;
    }
}

fn draw_actor(actor: &Actor, window: &pancurses::Window)
{
    window.mvprintw(
        LOCATION_DRAW_Y_OFFSET,
        LOCATION_DRAW_X_OFFSET + (actor.location_x * LOCATION_DRAW_X_SCALE),
        '@'.to_string(),
    );
}

struct MessageLog
{
    messages: [String; MESSAGE_LOG_CAPACITY],
    idx: usize,
    nr_messages: usize,
}

impl MessageLog
{
    fn new() -> MessageLog
    {
        MessageLog {
            messages: Default::default(),
            idx: 0,
            nr_messages: 0,
        }
    }
}

fn draw_message_log(log: &MessageLog, window: &pancurses::Window)
{
    let mut y = MESSAGES_DRAW_Y_OFFSET + (log.nr_messages as i32) - 1;

    let mut idx = log.idx;

    for _i in 0..log.nr_messages {
        let message = &log.messages[idx];
        window.mvprintw(y, MESSAGES_DRAW_X_OFFSET, message);

        if idx == 0 {
            idx = log.nr_messages - 1;
        } else {
            idx -= 1;
        }

        y -= 1;
    }
}

fn add_message(message: String, log: &mut MessageLog)
{
    if log.nr_messages > 0 {
        log.idx = (log.idx + 1) % MESSAGE_LOG_CAPACITY;
    }

    if log.nr_messages < MESSAGE_LOG_CAPACITY {
        log.nr_messages += 1;
    }

    log.messages[log.idx] = message;
}

struct Actor
{
    location_x: i32,
    // hp: i32,
}

struct Location
{
    terrain: Vec<char>,
    monsters: Vec<Actor>,
}

fn generate_location() -> Location
{
    let location = Location {
        terrain: vec!['.'; LOCATION_SIZE],
        monsters: vec![Actor { location_x: 15 }],
    };

    return location;
}

pub fn main()
{
    env_logger::init();
    info!("Starting");

    let window = pancurses::initscr();
    window.keypad(true);
    window.refresh();
    pancurses::noecho();
    pancurses::curs_set(0);

    let mut location = generate_location();

    let mut player = Actor { location_x: 0 };

    let mut message_log = MessageLog::new();

    // TODO: add_message should also accept &str
    add_message("1".to_string(), &mut message_log);
    add_message("2".to_string(), &mut message_log);
    add_message("3".to_string(), &mut message_log);
    add_message("4".to_string(), &mut message_log);

    loop {
        window.erase();

        draw_terrain(&location.terrain, &window);
        for mon in &location.monsters {
            draw_actor(&mon, &window);
        }
        draw_actor(&player, &window);
        draw_message_log(&message_log, &window);

        match window.getch() {
            Some(input) => match input {
                Input::Character('q') => break,
                Input::Character('a') | Input::KeyLeft => {
                    if player.location_x > 0 {
                        player.location_x -= 1
                    }
                }
                Input::Character('d') | Input::KeyRight => {
                    let location_max_x = (location.terrain.len() as i32) - 1;

                    if player.location_x == location_max_x {
                        location = generate_location();
                        player.location_x = 0;
                    } else {
                        player.location_x += 1
                    }
                }
                _ => {}
            },
            _ => {}
        }

        pancurses::napms(50);
    }

    pancurses::endwin();

    info!("Bye!")
}
